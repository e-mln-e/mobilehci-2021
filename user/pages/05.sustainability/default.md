---
title: Sustainability
---

Some of the actions we plan for this are: 

1. **Committee meetings:** they will be organised remotely to reduce our carbon footprint. 
2. **Local transportation:** Toulouse is easy to navigate by foot and bicycle. We will be including access to local transportation into the conference registration. The local bus system and rental bikes will be available for the conference participants.
3. **Food:** Vegetarian food will be the default option at the conference. We will strive that most of the food served will be local, seasonal and organic.
4. **Waste:** We will aim to minimize waste of the conference. There will be a very low amount of printable and all prints will be on 100% recycling paper. There will be no disposable dishes, all glass bottles are part of a bottle return system. Germany has also a very good separate waste collection and a high recycling rate for glass, paper and compost.
5. **Topics:** We welcome submissions with mobile interaction approaches supporting a more sustainable conference organization and mobile life.