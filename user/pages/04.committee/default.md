---
title: Committee
media_order: 'alix_goguey.png,Anastasia.png,Dubois.jpg,AndreaBianchi.jpg,Anke Brock.jpg,Barrett_Ens.jpg,DanielaBusse.jpg,Florian.jpg,IMG_2946.jpg,JessicaCauchard.jpg,klen_copic_pucihar.jpg,k_spiel.jpg,MarcosSerrano.jpg,Martin Pielot.jpg,Matjazn.jpg,Pietrzak.jpg,portrait.jpg,Roudaut.png,Sandra-Bardot.png'
---

## General Chairs	
[safe-email autolink="true" icon="envelope"]chair2021@acm.mobilehci.org[/safe-email]
[two-columns]
[column]
[chair-img name="Marcos Serrano" affiliation="IRIT - University of Toulouse, France"]![Portrait of Marcos Serrano](MarcosSerrano.jpg)[/chair-img]
[/column]
[column]
[chair-img name="Jessica Cauchard" affiliation="Ben Gurion University of the Negev, Israel"]![Portrait of Jessica Cauchard](JessicaCauchard.jpg)[/chair-img]
[/column]
[/two-columns]

## PC chairs
[safe-email autolink="true" icon="envelope"]program2021@acm.mobilehci.org[/safe-email]
[two-columns][column]
[chair-img name="Anne Roudaut" affiliation="University of Bristol, UK"]![Portrait of Anne Roudaut](Roudaut.png)[/chair-img]
[/column][column]
[chair-img name="Sebastian Boring" affiliation="Aalborg University, Denmark"]![Portrait of Sebastian Boring](MarcosSerrano.jpg)[/chair-img]
[/column][/two-columns]

## Publicity/Social Media/Web
[safe-email autolink="true" icon="envelope"]web2021@acm.mobilehci.org[/safe-email]
[two-columns][column]
[chair-img name="Émeline Brulé" affiliation="University of Sussex, UK"]![Portrait of Marcos Serrano](portrait.jpg)[/chair-img]
[/column][column]
[chair-img name="Katta Spiel" affiliation="TU Wien, Austria"]![Portrait of Katta Spiel](k_spiel.jpg)[/chair-img]
[/column][/two-columns]

## Local
[safe-email autolink="true" icon="envelope"]local2021@acm.mobilehci.org[/safe-email]
[two-columns][column]
[chair-img name="Emmanuel Dubois" affiliation="IRIT - University of Toulouse, France "]![Portrait of Emmanuel Dubois](Dubois.jpg)[/chair-img]
[/column][/two-columns]

## Poster
[safe-email autolink="true" icon="envelope"]posters2021@acm.mobilehci.org[/safe-email]
[two-columns][column]
[chair-img name="Andrea Bianchi" affiliation="KAIST, Korea"]![Portrait of Andrea Bianchi](AndreaBianchi.jpg)[/chair-img]
[/column][column]
[chair-img name="Anastasia Kuzminykh" affiliation="University of Waterloo, Canada"]![Portrait of Anastasia Kuzminykh](Anastasia.png)[/chair-img]
[/column][/two-columns]

## Demo
[safe-email autolink="true" icon="envelope"]demos2021@acm.mobilehci.org[/safe-email]

[two-columns][column]
[chair-img name="Florian Alt" affiliation="Bundeswehr University, Germany"]![Portrait of Florian Alt](Florian.jpg)[/chair-img]
[/column][column]
[chair-img name="Barrett Ens" affiliation="Monash University, Australia"]![Portrait of Barrett Ens](Barrett_Ens.jpg)[/chair-img]
[/column][/two-columns]



## Tutorial	
[safe-email autolink="true" icon="envelope"]tutorials2021@acm.mobilehci.org[/safe-email]
[two-columns][column]
[chair-img name="Daniela Busse" affiliation="eBay, USA"]![Portrait of Daniela Busse](DanielaBusse.jpg)[/chair-img]
[/column][column]
[chair-img name="Andrés Lucero" affiliation="Aalto University, Finland"]![Portrait of Andrés Lucero](Barrett_Ens.jpg)[/chair-img]
[/column][/two-columns]


## Sponsors
[safe-email autolink="true" icon="envelope"]sponsor2021@acm.mobilehci.org[/safe-email]
[two-columns][column]
[chair-img name="Martin Pielot" affiliation="Google, Germany"]![Portrait of Martin Pielot](Martin%20Pielot.jpg)[/chair-img]
[/column][column]
[chair-img name="Wendy Ju" affiliation="Cornell Tech, USA"]![Portrait of Wendy Ju](Barrett_Ens.jpg)[/chair-img]
[/column][/two-columns]

## Doctoral Consortium
[safe-email autolink="true" icon="envelope"]dc2021@acm.mobilehci.org[/safe-email]
[two-columns][column]
[chair-img name="Xing-Dong Yang" affiliation="Dartmouth College, USA"]![Portrait of Xing-Dong Yang](IMG_2946.jpg)[/chair-img]
[/column][column]
[chair-img name="Wendy Ju" affiliation="Cornell Tech, USA"]![Portrait of Wendy Ju](Barrett_Ens.jpg)[/chair-img]
[/column][/two-columns]

## Workshop
[safe-email autolink="true" icon="envelope"]workshops2021@acm.mobilehci.org[/safe-email]
[two-columns][column]
[chair-img name="Matjaz Kljun" affiliation="University of Primorska, Slovenia"]![Portrait of Matjaz Kljun](Matjazn.jpg)[/chair-img]
[/column][column]
[chair-img name="Klen Čopič Pucihar" affiliation="University of Primorska, Slovenia"]![Portrait of Klen Čopič Pucihar](klen_copic_pucihar.jpg)[/chair-img]
[/column][/two-columns]

## Panel
[safe-email autolink="true" icon="envelope"]panels2021@acm.mobilehci.org[/safe-email]
[two-columns][column]
[chair-img name="Anicia Peters" affiliation="University of Namibia, Namibia"]![Portrait of Anicia Peters](Anke%20Brock.jpg)[/chair-img]
[/column][column]
[chair-img name="Joel Lanir" affiliation="University of Haifa, Israel"]![Portrait of Joel Lanir](alix_goguey.png)[/chair-img]
[/column][/two-columns]
        
## Student Design Competition
[safe-email autolink="true" icon="envelope"]sdc2021@acm.mobilehci.org[/safe-email]
[two-columns][column]
[chair-img name="Eva Hornecker" affiliation="Bauhaus-Universität Weimar, Germany"]![Portrait of Eva Hornecker](Anke%20Brock.jpg)[/chair-img]
[/column][column]
[chair-img name="David Sirkin" affiliation="Stanford University, USA"]![Portrait of David Sirkin](alix_goguey.png)[/chair-img]
[/column][/two-columns]
        
## Accessibility
[safe-email autolink="true" icon="envelope"]access2021@acm.mobilehci.org[/safe-email]
[two-columns][column]
[chair-img name="Anke Brock" affiliation="ENAC - University of Toulouse	France"]![Portrait of Anke Brock](Anke%20Brock.jpg)[/chair-img]
[/column][column]
[chair-img name="João Guerreiro" affiliation="University of Lisbon, Portugal"]![Portrait of João Guerreiro](alix_goguey.png)[/chair-img]
[/column][/two-columns]
        
## Sustainability 
[safe-email autolink="true" icon="envelope"]sustain2021@acm.mobilehci.org[/safe-email]
[two-columns][column]
[chair-img name="Sandra Bardot" affiliation="University of Manitoba, Canada"]![Portrait of Sandra Bardot](Sandra-Bardot.png)[/chair-img]
[/column][column]
[chair-img name="Alix Goguey" affiliation="Grenoble Alpes University, France"]![Portrait of Alix Goguey](alix_goguey.png)[/chair-img]
[/column][/two-columns]

## Publication	
[safe-email autolink="true" icon="envelope"]publications2021@acm.mobilehci.org[/safe-email]
[two-columns][column]
[chair-img name="Mathieu Raynal" affiliation="IRIT - University of Toulouse, France"]![Portrait of Mathieu Raynal](Sandra-Bardot.png)[/chair-img]
[/column][column]
[chair-img name="Thomas Pietrzak" affiliation="University of Lille, France"]![Portrait of Thomas Pietrzak](Pietrzak.jpg)[/chair-img]
[/column][/two-columns]

