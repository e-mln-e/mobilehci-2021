---
title: Home
media_order: banner.svg
---

[banner title="Welcome to MobileHCI 2021!" date="September 27–30, 2021" place="Toulouse, France"] [/banner]
[two-columns]
[column]
After over 20 years of shaping research, development and practice in mobile devices and services, MobileHCI updates its name and becomes the ACM International Conference on Mobile Human-Computer Interaction. As we enter a new decade, this conference series is adapting to reflect the societal and technological transition where mobility has become pervasive and prime to our lives.
    
Our community and work is ever more relevant as we bring together academics, designers, and practitioners from multiple disciplines to discuss the challenges and future of people interacting with and through technologies, applications, and services in a mobile world. 
[/column]
[column]
Attendees will hear from world-leading experts in research talks and workshops; see, touch, and feel new mobile experiences in our demo and poster sessions; as well as be inspired by industry and academic thought-leaders during our panels; while still having time to network and form future collaborations.
        
MobileHCI 2021 welcomes contributions related to any aspect of mobile Human-Computer Interaction, from technology, to user experience, methodology, theoretical contributions, and beyond. Our conference also solicits proposals for workshops, demonstrations, tutorials, and industrial case study papers. We look forward to seeing you in Toulouse in September  2021!
[/column]
[/two-columns]

## Upcoming Deadlines and news
[two-columns]
[column]
[call-to-action]
### Important deadlines
|  |  |
| ------ | ----------- |
| Paper deadline    | February 4th | 
| Final notifications (post rebuttal)    | May 10th | 
| All other tracks notifications    | May 21st |
| All other tracks deadline    | June 18th |
| All other tracks camera ready    | June 30th |
[/call-to-action]
[/column]

[column]
<a class="twitter-timeline" data-height="300" href="https://twitter.com/ACMMobileHCI?ref_src=twsrc%5Etfw">Tweets by ACMMobileHCI</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
[/column]
[/two-columns]

