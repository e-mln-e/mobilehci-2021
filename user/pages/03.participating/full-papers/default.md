---
title: 'Call for full tpapers'
---

[two-columns]
[column]"Expanding the Horizon of Mobile Interaction"
We welcome contributions related to any aspect of mobile HCI technology, systems, devices, techniques, experience, application, methods, tools, theories, and new perspectives. Our interpretation of mobility is inclusive and broadly construed.
[/column]
[column]
[call-to-action]
|  |  |
| ------ | ----------- |
| Paper deadline    | February 4th Everywhere on Earth | 
| Final notifications (post rebuttal)    | May 10th | 
| Camera-ready    | May 21st |
[/call-to-action]
[/column]
[/two-columns]

#### Suggested Topics
Systems and Infrastructure
The design, architecture, deployment, and evaluation of systems and infrastructures that support development of or interaction with mobile devices and services.

Devices and Techniques
The design, construction, usage, and evaluation of devices and techniques that create valuable new capabilities for mobile human-computer interaction.

Applications and Experiences
Descriptions of the design, empirical study of interactive applications, or analysis of usage trends that leverage mobile devices and systems. 

MobileHCI for social good
Using mobile HCI to address societal challenges of our times such as health care, mobility, sustainability, social engagement, and civic engagement.

Methods and Tools
New methods and tools designed for or applied to studying or building mobile user interfaces, applications, and mobile users.

Theories and Models
Critical analysis or organizing theory with clearly motivated relevance to the design or study of mobile human-computer interaction; taxonomies of design or devices; well-supported essays on emerging trends and practice in mobile human-computer interaction.

Provocations and new Perspectives
Well-argued and well-supported visions of the future of mobile computing; non-traditional topics that bear on mobility; underrepresented viewpoints and perspectives that convincingly bring something new to mobile research and practice.

### Submission Information
#### Submission Platform	
All materials must be submitted electronically to PCS 2.0 http://new.precisionconference.com/~sigchi by the abstract and paper deadline
In PCS 2.0, first click “Submissions” at the top of the page, from the dropdown menus for society, conference, and track select “SIGCHI”, “UIST 2020” and “UIST 2020 Papers”, respectively, and press “Go”.
#### Submission Format and Length	
MobileHCI Full Paper Format:
Word Template | Latex Template | Overleaf Template
typical page length is 4-10 pages + references, but any page length is allowed
page length should be commensurate with contribution size
#### Maximum File Size	
maximum total size for all files < 100 MB
#### Anonymity	
anonymous, i.e. authors must remove their names and affiliations throughout all submission materials (paper, videos, supplementary material) [anonymity policy]
#### Selection process	
referred, i.e. the program committee and a set of external reviewers will review submitted papers
#### Publication	
archival, i.e. accepted papers will be published in the ACM Digital Library and distributed in digital form to conference attendees
Presentation Format	
talk at the conference by one or more authors of the paper
20 minute talk (15 min + 5 min Q&A)
#### Demo	
authors of accepted papers will be invited to participate in the UIST demo session [see Demos]

#### Before Submitting, Read General Guidelines
Below we highlight some of the most important points, but please ensure you have reviewed the UIST paper submission and review process [review process guide] as well as a guide to a successful UIST paper submission [general guidelines].

**Original Contributions & Novelty Over Prior Work:** If you have previously published a related paper, are unsure if your contribution is large enough to warrant a full paper, or if your work builds on top of your own prior poster or demo, please read the guidelines on novelty over existing work [novelty guidelines].
**Citing Your Own Prior Work:** UIST has strict guidelines on how to refer to your own prior work, including your own prior papers, posters, and demos at UIST and other venues [prior work guidelines].
**Concurrent Submission:** MobileHCI does not allow any concurrent submissions to other conferences or journals during the entire review process [concurrent submission policy].

[two-columns][column]
[chair-img name="Anne Roudaut" affiliation="University of Bristol, UK"]![Portrait of Marcos Serrano](https://hci.cs.umanitoba.ca/assets/images/people/_bio/marcos.jpg)[/chair-img]
[/column][column]
[chair-img name="Sebastian Boring" affiliation="Aalborg University, Denmark"]![Portrait of Marcos Serrano](https://hci.cs.umanitoba.ca/assets/images/people/_bio/marcos.jpg)[/chair-img]
[/column][/two-columns]