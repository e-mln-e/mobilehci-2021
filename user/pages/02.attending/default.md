---
title: Attending
---

## Registration
Details to be announced.

[two-columns]
[column]
## Venue
MobileHCI’21 will be held at the Centre de Congrès Pierre Baudis. The convention centre is situated in a privileged environment at the heart of the city and surrounded by the green parks of the "Jardin de Compans Caffarelli".

### Access:
There are metro, bus, and streetcar access and the airport shuttle right in front of the convention centre.
* BY METRO: The line B stop, Compans Caffarelli, is located 1 minute by foot from the Centre and operates Sunday to Thursday from 5:15 a.m. to midnight, and Friday and Saturday till 2:00 a.m.
* BY BUS: Lines 16 - 31 - 45 - 63 stop at the convention centre.
* BY STREETCAR: Line B stops at Compans Caffarelli.
[/column]
[column]
## Accommodation

Visitors of Toulouse can choose among many accomodation offers:


**Widget on the website from Toulouse Convention Center + introduction title ??**
[/column]
[/two-columns]
## About Toulouse
[two-columns]
[column]
Toulouse is the capital of the French department of Haute-Garonne and of the Occitania region. It is located within a privileged natural surroundings on the banks of the River Garonne, 150 km (93 mi) from the Mediterranean Sea, 230 km (143 mi) from the Atlantic Ocean, 110 km (68 mi) from the Pyrenees mountains, and 680 km (420 mi) south of Paris. It is the fourth-largest city in France, with 1.331.000 inhabitants (metropolitan area), after Paris, Lyon and Marseille.

Toulouse is the centre of the European aeronautic & spatial industries, with the headquarters of Airbus and the largest european space centre: Toulouse Space Centre (CST), belonging to the French National Centre for Space Studies (CNES). Toulouse is also the first European pole for biotechnologies and life science and the 2nd research hub in France, with more than 400 laboratories and 110,000 researchers. The University of Toulouse, founded in 1229, is the fourth largest in France, and one of the oldest in Europe.
[/column]
[column]
Toulouse boasts a unique architecture made of pink terracotta bricks, which earned the nickname la Ville Rose ("the Pink City"). The city seduces by its small sinuous streets and diverse medieval to modern monuments. It is a cosmopolitan and enthusiastic town, mixing historical heritage and modern lifestyle.

Toulouse counts two UNESCO World Heritage Sites, the Canal du Midi (designated in 1996 and shared with other cities), and the Basilica of St. Sernin, the largest remaining Romanesque building in Europe, designated in 1998 because of its significance to the Santiago de Compostela pilgrimage route.
[/column]
[/two-columns]

Toulouse [features unique monuments and museums](https://www.meetings-toulouse.com/sites/www.meetings-toulouse.fr/files/atoms/files/plan-touristique-toulouse.pdf), such as:
* Capitole: this imposing building, which was completed in the 18th century, is home to both the Town Hall and the Capitole Theatre. 
Saint-Sernin Basilica: listed as a World Heritage Site (on the way of Saint James), this monument is considered one of the biggest romanesque basilicas in the West. 
* Jacobins Monastic Ensemble: founded in the 13th and 14th centuries by the Dominican Order, this set of buildings is a jewel of meridional Gothic art. 
* Canal du Midi : Listed as a UNESCO World Heritage Site, the Canal du Midi runs from Toulouse to the Mediterranean Sea. 
* Private mansions : during the Renaissance, the city, enriched by the woad trade, saw a blossoming of sumptuous homes. Later on, the classic era was to make a lasting mark on the mansions spread throughout the city. 
* Cité de l’Espace and Airbus industry. 
* Fine Arts Museums, such as: the Augustins fine arts museum, the Natural History museum, and the Abattoirs museum of modern and contemporary art. 

More information can be found at: [https://www.toulouse-visit.com/](https://www.toulouse-visit.com/)

## Funding for students
If you’re a student who wants to attend the MobileHCI 2021 and need help funding your travel, you can apply for one of the following travel grants.
[two-columns]
[column]
### Gary Marsden Student Development Fund
In recognition of Gary Marsden’s contributions and inspiration in HCI4D, ACM SIGCHI established Gary Marsden Student Development Fund in July 2015. The purpose is to support growth of HCI in developing worlds by sponsoring SIGCHI student members who are postgraduate students from and currently based in developing countries to attend HCI relevant events. For those who are currently not student members of SIGCHI, you can [apply online](https://services.acm.org/public/qj/gensigqj/gensigqj_control.cfm?promo=QJSIG&form_type=SIG&offering=026&sigstu=SIG_STUD&byp=1&CFID=429048&CFTOKEN=77335301) to become a member before submitting the application.
[More information about the Gary Marsden Student Development Fund](https://sigchi.org/get-involved/community_support/gary-marsden-student-development-fund/).

**Note: The application submission deadlines are 15th of February, May, August and November each year.**
[/column]
[column]
### SIGCHI Student Travel Grant
The SIGCHI Student Travel Grant (SSTG) program is intended to enable students who **lack other support opportunities** to attend CHI and other SIGCHI sponsored or co-sponsored conferences. This travel grant is intended to support students whose intention is to **present** at MobileHCI 2020, not just attend. The goal is to pre-approve students for grants **before** a conference’s earliest submission deadline so that if a student gets a submission accepted, that student can count on having a grant awarded for travel to the conference.

[More information about the SSTG program](https://sigchi.org/conferences/student-travel-grants/sigchi-student-travel-grant/).
[/column]
[/two-columns]

## Visa Letter
The ACM is able to provide visa support letters to all MobileHCI 2021 attendees as well as authors with accepted papers, posters, or members of the conference committee. 

For visa support letters, refer all requests to [supportletters@acm.org](mailto:supportletters@acm.org).  Please allow up to 10 business days to receive a letter. All requests are handled in the order they are received. The information below should be included with the request:
1. Your name as it appears on your passport
2. Your current postal mailing address
3. The name of the conference you are registering for
4. Your registration confirmation number
5. If you have any papers accepted for the conference, please provide the title and indicate whether you are the “sole author” or a “co-author”
6. Authors may indicate their paper title. If no paper, speakers can provide the title of their presentation

ACM does not provide letters for transport of vendor or presenter equipment.  
ACM suggests shipping the materials insured to the conference facility.
