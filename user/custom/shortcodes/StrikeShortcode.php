<?php
namespace Grav\Plugin\Shortcodes;

use Thunder\Shortcode\Shortcode\ShortcodeInterface;

class StrikeShortcode extends Shortcode
{
    public function init()
    {
        $this->shortcode->getHandlers()->add('strike', function(ShortcodeInterface $sc) {
            return '<del>'.$sc->getContent().'</del>';
        });
    }
}