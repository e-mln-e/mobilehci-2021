<?php
namespace Grav\Plugin\Shortcodes;

use Thunder\Shortcode\Shortcode\ShortcodeInterface;

class CallToAction extends Shortcode
{
    public function init()
    {
        $this->shortcode->getHandlers()->add('call-to-action', function(ShortcodeInterface $sc) {
            return '<div class="call-to-action">'.$sc->getContent().'</div>';
        });
    }
}