<?php
namespace Grav\Plugin\Shortcodes;

use Thunder\Shortcode\Shortcode\ShortcodeInterface;

class ChairPersonImg extends Shortcode
{
    public function init()
    {
        $this->shortcode->getHandlers()->add('chair-img', function(ShortcodeInterface $sc) {
            return '<div class="chair-person">'.$sc->getContent().'
                    <p><span class="chair-person-name">'.$sc->getParameter('name', 'Chair name').'</span><br>'.$sc->getParameter('affiliation', 'University, City').'</p>
                </div>';
        });
    }
}