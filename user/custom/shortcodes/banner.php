<?php
namespace Grav\Plugin\Shortcodes;

use Thunder\Shortcode\Shortcode\ShortcodeInterface;

class banner extends Shortcode
{
    public function init()
    {
        $this->shortcode->getHandlers()->add('banner', function(ShortcodeInterface $sc) {
            return '<div class="banner">'.$sc->getContent().'
            	<div>
	            	<h5>'.$sc->getParameter('date', 'Month number, year').'</h5>
	            	<h1>'.$sc->getParameter('title', 'MobileHCI 2021').'</h1>
	            	<p>'.$sc->getParameter('place', 'City, Country').'</p>
	            </div>
            </div>
            <div class="banner-margin"></div>';
        });
    }
}