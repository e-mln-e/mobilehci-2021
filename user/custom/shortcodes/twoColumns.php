<?php
namespace Grav\Plugin\Shortcodes;

use Thunder\Shortcode\Shortcode\ShortcodeInterface;

class twoColumns extends Shortcode
{
    public function init()
    {
        $this->shortcode->getHandlers()->add('two-columns', function(ShortcodeInterface $sc) {
            return '<div class="two-columns">'.$sc->getContent().'</div>';
        });
    }
}