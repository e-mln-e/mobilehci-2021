<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* partials/logo.html.twig */
class __TwigTemplate_8cbdbd3d93737c247b5f53d4b2da4001408d44073b2b1007f3920ca8f7314333 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "<a href=\"";
        echo ($context["home_url"] ?? null);
        echo "\" class=\"navbar-brand mr-10\">
  <h1 class=\"mobilehci-title\">";
        // line 2
        echo $this->getAttribute(($context["site"] ?? null), "title", []);
        echo "</h1>
</a>";
    }

    public function getTemplateName()
    {
        return "partials/logo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  35 => 2,  30 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("<a href=\"{{ home_url }}\" class=\"navbar-brand mr-10\">
  <h1 class=\"mobilehci-title\">{{ site.title }}</h1>
</a>", "partials/logo.html.twig", "/Users/etb20/Documents/RESEARCH/2020/Service/grav-clean/user/themes/mobile-hci/templates/partials/logo.html.twig");
    }
}
