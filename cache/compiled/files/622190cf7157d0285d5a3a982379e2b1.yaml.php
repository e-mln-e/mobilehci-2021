<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/Users/etb20/Documents/RESEARCH/2020/Service/grav-clean/user/plugins/markdown-notices/markdown-notices.yaml',
    'modified' => 1591763832,
    'data' => [
        'enabled' => true,
        'built_in_css' => true,
        'base_classes' => 'notices',
        'level_classes' => [
            0 => 'yellow',
            1 => 'red',
            2 => 'blue',
            3 => 'green'
        ]
    ]
];
