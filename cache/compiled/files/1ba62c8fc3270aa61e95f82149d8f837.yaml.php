<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/Users/etb20/Documents/RESEARCH/2020/Service/grav-clean/user/config/site.yaml',
    'modified' => 1593298458,
    'data' => [
        'title' => 'MobileHCI 2021',
        'default_lang' => 'en',
        'author' => [
            'name' => 'MobileHCI',
            'email' => 'joe@example.com'
        ],
        'taxonomies' => [
            0 => 'category',
            1 => 'tag'
        ],
        'metadata' => [
            'description' => 'MobileHCI is a leading conference part of the SIGCHI family'
        ],
        'summary' => [
            'enabled' => true,
            'format' => 'short',
            'size' => 300,
            'delimiter' => '==='
        ],
        'redirects' => NULL,
        'routes' => NULL,
        'blog' => [
            'route' => '/blog'
        ]
    ]
];
