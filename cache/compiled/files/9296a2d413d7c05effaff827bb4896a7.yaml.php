<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/Users/etb20/Documents/RESEARCH/2020/Service/grav-clean/user/accounts/emeline.yaml',
    'modified' => 1593219490,
    'data' => [
        'state' => 'enabled',
        'email' => 'e.t.brule@sussex.ac.uk',
        'fullname' => 'Emeline',
        'title' => 'Administrator',
        'access' => [
            'admin' => [
                'login' => true,
                'super' => true
            ],
            'site' => [
                'login' => true
            ]
        ],
        'hashed_password' => '$2y$10$TEC3O0LfqdUut3p11cxZE.VIqvR8Y4ibqiEfHFn3CUdtWlTggXLGS'
    ]
];
