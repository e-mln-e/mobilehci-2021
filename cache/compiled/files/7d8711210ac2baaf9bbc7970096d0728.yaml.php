<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/Users/etb20/Documents/RESEARCH/2020/Service/grav-clean/user/plugins/problems/languages.yaml',
    'modified' => 1591763832,
    'data' => [
        'en' => [
            'PLUGIN_PROBLEMS' => [
                'BUILTIN_CSS' => 'Use built in CSS',
                'BUILTIN_CSS_HELP' => 'Include the CSS provided by the Problems plugin'
            ]
        ],
        'ru' => [
            'PLUGIN_PROBLEMS' => [
                'BUILTIN_CSS' => 'Использовать встроенный CSS',
                'BUILTIN_CSS_HELP' => 'Использовать CSS, предоставленный плагином Problems'
            ]
        ],
        'uk' => [
            'PLUGIN_PROBLEMS' => [
                'BUILTIN_CSS' => 'Використовувати вбудований CSS',
                'BUILTIN_CSS_HELP' => 'Використовувати CSS, наданий плагіном Problems'
            ]
        ]
    ]
];
