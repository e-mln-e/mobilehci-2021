<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => 'plugins://devtools/devtools.yaml',
    'modified' => 1593295879,
    'data' => [
        'enabled' => true,
        'collision_check' => true
    ]
];
