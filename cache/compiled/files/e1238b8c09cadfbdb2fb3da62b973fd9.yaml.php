<?php
return [
    '@class' => 'Grav\\Common\\File\\CompiledYamlFile',
    'filename' => '/Users/etb20/Documents/RESEARCH/2020/Service/grav-clean/user/plugins/shortcode-core/shortcode-core.yaml',
    'modified' => 1593219620,
    'data' => [
        'enabled' => true,
        'active' => true,
        'active_admin' => true,
        'admin_pages_only' => true,
        'parser' => 'regular',
        'include_default_shortcodes' => true,
        'custom_shortcodes' => NULL,
        'fontawesome' => [
            'load' => true,
            'url' => '//maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css',
            'v5' => false
        ]
    ]
];
